var express = require('express');
var fs = require('fs');
var logger = require('morgan');
var cheerio = require('cheerio');
var cors = require('cors');
var app     = express();

var Nightmare = require('nightmare');       
//var Horseman = require('node-horseman');
var request = require('request');



app.use(logger('dev'));
app.use(cors());

app.get('/health', function(req, res){
    res.status(200).send('yay!');
});


app.get('/scrape', function(req, res){
    // The URL we will scrape from - in our example Anchorman 2.

    var username = req.query.username;

    if(username === undefined){
        username = 'ninja';
    }

    console.log('Get URL');

    var url = 'https://www.h1z1.com/king-of-the-kill/leaderboards/pre-season-3?region=1&pageLength=25&page=1&searchText=' + username;

    console.log(url);

    var nightmare = Nightmare();
    nightmare
      .goto(url)
      .wait('#leaderboardTable tbody tr.odd')
      .click('#leaderboardTable tbody tr:first-child')
      .wait('#leaderboardTable .player-details-row')
      .evaluate(function () {
        return document.querySelector('html').outerHTML;
      })
      .end()
      .then(function (result) {
        console.log('RESULT');
        //console.log(result);

        var $ = cheerio.load(result);

        var detail = $('#leaderboardTable tbody .player-details-row').html();
        console.log('detail');
        console.log(detail);
        console.log('success');
        res.status(200).send(detail);

      })
      .catch(function (error) {
        console.error('Error:', error);
        res.status(500).send(error);
      });
});


app.get('/test', function(req, res){
  
  request('https://www.h1z1.com/king-of-the-kill/leaderboards/pre-season-3?region=1&pageLength=25&page=1&searchText=ninja', function (error, response, body) {
    if (!error && response.statusCode == 200) {
      console.log(body) // Show the HTML for the Google homepage.
      res.send(body);
    }
  });

});




//var server = require('http').Server(app);

// TESTING LOCALLY
var server_port = 80;
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';

// OPENSHIFT PORT AND IP
if(process.env.OPENSHIFT_NODEJS_PORT && process.env.OPENSHIFT_NODEJS_IP){
  server_port = process.env.OPENSHIFT_NODEJS_PORT;
  server_ip_address = process.env.OPENSHIFT_NODEJS_IP;
}


// PRODUCTION (AWS)
// if(process.env.NODE_ENV === 'production'){
//   // use the production database
//   var server_port = 3000;
//   var server_ip_address = '127.0.0.1';
// }

// PROD
// if(process.env.PORT === '8081'){
//   server_port = 8080;
//   server_ip_address = '127.0.0.1';
// }



console.log('LISTENING');
app.listen(server_port, function () {
  console.log( "Listening on " + server_ip_address + ", server_port " + server_port );
}); 

//require('./sockets/sockets').listen(server);
exports = module.exports = app;









